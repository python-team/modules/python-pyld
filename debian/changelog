python-pyld (2.0.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Use pybuild-plugin-pyproject.
  * Policy version 4.7.0: no changes required.

 -- Colin Watson <cjwatson@debian.org>  Sun, 26 May 2024 12:52:10 +0100

python-pyld (2.0.3-3) unstable; urgency=medium

  * Update standards version to 4.6.1
  * Add myself to debian/ copyright

 -- James Valleroy <jvalleroy@mailbox.org>  Sun, 31 Jul 2022 07:07:59 -0400

python-pyld (2.0.3-2) unstable; urgency=medium

  * Update standards version to 4.5.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 25 May 2022 19:19:52 +0100

python-pyld (2.0.3-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Add debian/watch file, using pypi.

  [ James Valleroy ]
  * d/control: Add Rules-Requires-Root
  * d/control: Use debhelper compat version 13
  * d/control: Add sphinxdoc Depends
  * d/control: Update standards version to 4.5.0
  * d/control: Adopt orphaned package (Closes: #894148)
  * New upstream version 2.0.3
  * d/control: Update dependencies
  * Cleanup build of obsolete docs

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Jonathan Carter ]
  * Fix changelog entry merging Ondřej's entry with previously unreleased
    changelog

 -- James Valleroy <jvalleroy@mailbox.org>  Thu, 17 Sep 2020 22:26:37 -0400

python-pyld (0.6.8-2) unstable; urgency=medium

  * QA upload.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Fix Format URL to correct one
  * d/control: Deprecating priority extra as per policy 4.0.1
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Piotr Ożarowski ]
  * Add dh-python to Build-Depends

  [ Andrey Rahmatullin ]
  * Drop Python 2 support.
  * Set the Maintainer field to the QA Group.

 -- Andrey Rahmatullin <wrar@debian.org>  Tue, 20 Aug 2019 00:41:20 +0500

python-pyld (0.6.8-1) unstable; urgency=low

  * New upstream version.

 -- W. Martin Borgert <debacle@debian.org>  Tue, 13 Oct 2015 22:46:31 +0000

python-pyld (0.6.2-1) unstable; urgency=low

  * Initial release (Closes: #764543)

 -- W. Martin Borgert <debacle@debian.org>  Thu, 09 Oct 2014 08:48:22 +0000
